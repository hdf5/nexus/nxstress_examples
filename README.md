# NXstress

Documentation of the new _NXstress application definition_:

https://hdf5.gitlab-pages.esrf.fr/nexus/nxstress_examples/classes/contributed_definitions/NXstress.html#nxstress

When ready we will submit it to the official _NeXus standard_:

https://github.com/nexusformat/definitions/pull/1039

To run the script, you need to install all requirements

```bash
python3 -m pip install -r requirements.txt
```

## Examples

Generate _NXstress_ formatted data from raw data by running a python script

```bash
python3 generate.py --example
```

Visualize the _NXstress_ formatted HDF5 files

- https://h5web.panosc.eu/h5grove?file=easistress/esrf_id15_example2.h5
- https://h5web.panosc.eu/h5grove?file=easistress/ill_salsa_example1.h5

## Conversion to NXstress format

`generate.py` can be used to convert easistrain fit data to NXstress. For this, provide filenames to the script:

```bash
python3 generate.py fit1.h5 fit2.h5
```

This will create a `nxstress.h5` file in the directory of `fit1.h5`. If the experiment was run with the Eulerian cradle, the `--with_cradle` argument must be supplied

```bash
python3 generate.py fit1.h5 fit2.h5 --with_cradle
```

Sample name and experiment identifiers can be supplied to further tune the NXstress output:

```bash
python3 generate.py fit1.h5 fit2.h5 --sample Ni_ring --exp_identifier ihme19
```
