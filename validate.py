import h5py
import numpy
from easistrain.EDD import constants

ID15_d0 = {
    (2, 2, 0): 1.267594775,
    (3, 1, 1): 1.080594133,
    (2, 2, 2): 1.034951928,
    (3, 3, 1): 0.822132144,
    (4, 2, 0): 0.801651573,
}
ID15_ttheta_hor = 5.02876
ID15_ttheta_vert = 4.91329


def rotation_matrix(axis, angle):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by angle degrees.
    """
    axis = numpy.asarray(axis)
    axis = axis / norm(axis)
    angle = numpy.radians(angle)
    a = numpy.cos(angle / 2.0)
    b, c, d = -axis * numpy.sin(angle / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    M = numpy.array(
        [
            [aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
            [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
            [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc],
        ]
    )
    return M


def beam_in_sample_frame(scan):
    beam_in_labframe = numpy.array([1, 0, 0])
    labtosample = get_rotation(scan["sample"], inverse=True)
    return labtosample.dot(beam_in_labframe)


def dethor_in_sample_frame(scan):
    M = get_rotation(scan["instrument/mca2_det0"])
    det_in_labframe = M.dot([1, 0, 0])
    labtosample = get_rotation(scan["sample"], inverse=True)
    return labtosample.dot(det_in_labframe)


def detver_in_sample_frame(scan):
    M = get_rotation(scan["instrument/mca2_det1"])
    det_in_labframe = M.dot([1, 0, 0])
    labtosample = get_rotation(scan["sample"], inverse=True)
    return labtosample.dot(det_in_labframe)


def get_rotation(parent, inverse=False):
    """Orient an object in the laboratory frame"""
    M = numpy.eye(3)
    if inverse:
        for axis, angle in iter_rotations(parent):
            M = M.dot(rotation_matrix(axis, -angle))
    else:
        for axis, angle in iter_rotations(parent):
            M = rotation_matrix(axis, angle).dot(M)
    return M


def iter_rotations(parent):
    if "depends_on" not in parent:
        return
    name = parent["depends_on"][()]
    while name != ".":
        transfo = parent[name]
        if transfo.attrs.get("transformation_type") == "rotation":
            axis = transfo.attrs["vector"]
            angle = transfo[()]
            yield axis, angle  # active transformation
        name = transfo.attrs["depends_on"]
        parent = transfo.parent


def get_angle(v1, v2):
    v1 = v1 / norm(v1)
    v2 = v2 / norm(v2)
    return numpy.degrees(numpy.arccos(v1.dot(v2)))


def norm(v):
    return numpy.sqrt((v * v).sum(axis=0))


def assert_norm1(v, **kw):
    numpy.testing.assert_allclose(norm(v), 1, **kw)


def validate_file(filename: str):
    with h5py.File(filename, "r") as f:
        for scanname in f:
            scan = f[scanname]

            # Validate NXtransformations: angle between beam and detector in sample frame must be equal to 2theta
            vbeam = beam_in_sample_frame(scan)
            vhor = dethor_in_sample_frame(scan)
            vvert = detver_in_sample_frame(scan)
            assert_norm1(vbeam)
            assert_norm1(vhor)
            assert_norm1(vvert)
            ttheta_hor = get_angle(vbeam, vhor)
            ttheta_vert = get_angle(vbeam, vvert)
            numpy.testing.assert_allclose(ttheta_hor, ID15_ttheta_hor)
            numpy.testing.assert_allclose(ttheta_vert, ID15_ttheta_vert)

            # Validate Q vector: angle between Q and detector must be equal to 90-theta
            qx = scan["peaks/qx"][()]
            qy = scan["peaks/qy"][()]
            qz = scan["peaks/qz"][()]
            qall = numpy.asarray([qx, qy, qz])

            ttheta = qx * 0
            for i, qs in enumerate(qall.T):
                assert_norm1(qs)
                scatplane = numpy.cross(vbeam, qs)
                if abs(scatplane.dot(vhor)) < 1e-10:
                    tthetai = 2 * (90 - get_angle(qs, vhor))
                    numpy.testing.assert_allclose(ttheta_hor, tthetai)
                    ttheta[i] = tthetai
                elif abs(scatplane.dot(vvert)) < 1e-10:
                    tthetai = 2 * (90 - get_angle(qs, vvert))
                    numpy.testing.assert_allclose(ttheta_vert, tthetai)
                    ttheta[i] = tthetai
                else:
                    assert False

            # Validate energy: d-spacing more or less d0
            energy = scan["peaks/center"][()]
            hc = constants.pCstInkeVS * constants.speedLightInAPerS
            wavelength = hc / energy

            h = scan["peaks/h"][()]
            k = scan["peaks/k"][()]
            l = scan["peaks/l"][()]

            for h, k, l, wl, tt in list(zip(h, k, l, wavelength, ttheta)):
                d0 = ID15_d0[(h, k, l)]
                d = wl / (2 * numpy.sin(numpy.radians(tt / 2)))
                assert abs(d - d0) / d0 < 0.3


if __name__ == "__main__":
    validate_file("fmt_data/esrf_id15_example1.h5")
    validate_file("fmt_data/esrf_id15_example2.h5")
