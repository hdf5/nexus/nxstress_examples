import argparse
import sys
import os
from contextlib import contextmanager
from collections import OrderedDict
from datetime import datetime
from typing import Sequence
import h5py
import numpy
from easistrain.EDD.math import compute_qs

VLEN_DTYPE = h5py.special_dtype(vlen=str)


def as_vlen_string(lst):
    return numpy.array(lst, dtype=VLEN_DTYPE)


def h5url(h5obj):
    return f"{h5obj.filename}::{h5obj.name}"


def h5url_parse(url):
    filename, _, h5path = url.partition("::")
    if not h5path:
        h5path = "/"
    return filename, h5path


@contextmanager
def h5context(url):
    filename, h5path = h5url_parse(url)
    with h5py.File(filename, mode="r") as f:
        yield f[h5path]


def h5ls(url):
    with h5context(url) as parent:
        return [h5url(child) for child in parent]


def add_text_nxnote(parent, name, content):
    nxnote = parent.create_group(name)
    nxnote.attrs["NX_class"] = "NXnote"
    nxnote.attrs["type"] = "text/plain"
    nxnote["data"] = content


def add_nxprocess(parent, name, index=1):
    nxprocess = parent.create_group(name)
    parent.attrs["default"] = name
    nxprocess.attrs["NX_class"] = "NXprocess"
    nxprocess["sequence_index"] = index
    nxprocess["program"] = "easistrain"
    nxprocess["version"] = "1.0"
    nxprocess["date"] = datetime.now().astimezone().isoformat()
    add_text_nxnote(
        nxprocess, "description", "Some description about the fitting goes here ..."
    )


def id15_positioners(scan, with_cradle=True):
    positioners = scan["positioners"]
    if with_cradle:
        names = {
            "ex": ("x", lambda x: -x[()]),
            "ey": ("y", lambda x: -x[()]),
            "ez": ("z", lambda x: 9 - x[()]),
            "ephi": ("phi", lambda x: x[()]),
            "echi": ("chi", lambda x: x[()]),
            "sy": ("omega", lambda x: x[()]),
        }
    else:
        names = {
            "sx": ("x", lambda x: -x[()]),
            "sy": ("y", lambda x: -x[()]),
            "sz": ("z", lambda x: 9 - x[()]),
            "srz": ("phi", lambda x: x[()]),
            "ssy2": ("chi", lambda x: x[()]),
            "y42": ("omega", lambda x: x[()]),
        }
    result = dict()
    for name, (newname, func) in names.items():
        result[newname] = func(positioners[name])
    return result


def create_nxtransformation(nxtransformations, name, value, units, ttype, vector):
    dset = nxtransformations.create_dataset(name, data=value)
    if ttype is None:
        dset.attrs.update({"vector": vector})
    else:
        dset.attrs.update(
            {"transformation_type": ttype, "units": units, "vector": vector}
        )
    return dset


def add_coordinate_system(nxtransformations, dset):
    # beam: +X
    # gravity: -Z
    dset.attrs["depends_on"] = "beam"
    dset = create_nxtransformation(
        nxtransformations, "beam", float("nan"), None, None, [1, 0, 0]
    )
    dset.attrs["depends_on"] = "gravity"
    dset = create_nxtransformation(
        nxtransformations, "gravity", float("nan"), None, None, [0, 0, -1]
    )
    dset.attrs["depends_on"] = "."


def position_point_detector(nxdetector, distance, polar, azimuth):
    # X_lab =  Rx(-azimuth) . Ry(-polar) . Tx(distance) . X_sdd
    nxtransformations = nxdetector.create_group("position")
    nxtransformations.attrs["NX_class"] = "NXtransformations"
    nxdetector["depends_on"] = "position/distance"
    dset = create_nxtransformation(
        nxtransformations, "distance", distance, "cm", "translation", [1, 0, 0]
    )
    dset.attrs["depends_on"] = "polar"
    dset = create_nxtransformation(
        nxtransformations, "polar", polar, "deg", "rotation", [0, -1, 0]
    )
    dset.attrs["depends_on"] = "azimuth"
    dset = create_nxtransformation(
        nxtransformations, "azimuth", azimuth, "deg", "rotation", [1, 0, 0]
    )
    add_coordinate_system(nxtransformations, dset)


def position_point_detector_slits(nxslit, distance, polar, azimuth):
    # X_lab =  Rx(azimuth) . Ry(-polar) . Tx(distance) . Ry(90) . X_sdd
    nxtransformations = nxslit.create_group("position")
    nxtransformations.attrs["NX_class"] = "NXtransformations"
    nxslit["depends_on"] = "position/perpendicular"
    dset = create_nxtransformation(
        nxtransformations, "perpendicular", 90, "deg", "rotation", [0, 1, 0]
    )
    dset.attrs["depends_on"] = "distance"
    dset = create_nxtransformation(
        nxtransformations, "distance", distance, "cm", "translation", [1, 0, 0]
    )
    dset.attrs["depends_on"] = "polar"
    dset = create_nxtransformation(
        nxtransformations, "polar", polar, "deg", "rotation", [0, -1, 0]
    )
    dset.attrs["depends_on"] = "azimuth"
    dset = create_nxtransformation(
        nxtransformations, "azimuth", azimuth, "deg", "rotation", [1, 0, 0]
    )
    add_coordinate_system(nxtransformations, dset)


def position_source_slits(nxslit):
    # X_lab = Tx(-distance) . Ry(90) . X_sdd
    nxtransformations = nxslit.create_group("position")
    nxtransformations.attrs["NX_class"] = "NXtransformations"

    nxslit["depends_on"] = "position/perpendicular"
    dset = create_nxtransformation(
        nxtransformations, "perpendicular", 90, "deg", "rotation", [0, 1, 0]
    )
    dset.attrs["depends_on"] = "distance"
    dset = create_nxtransformation(
        nxtransformations, "distance", float("nan"), "cm", "translation", [-1, 0, 0]
    )
    add_coordinate_system(nxtransformations, dset)


def position_sample(nxsample, positioners):
    # X_lab = Ry(-omega) . Tz[z]  . Ty[y]  . Tx[x] . Rx(chi) . Rz(-phi) . X_sample
    nxtransformations = nxsample.create_group("position")
    nxtransformations.attrs["NX_class"] = "NXtransformations"

    nxsample["depends_on"] = "position/phi"
    dset = create_nxtransformation(
        nxtransformations,
        "phi",
        positioners["phi"],
        "deg",
        "rotation",
        [0, 0, -1],
    )
    dset.attrs["depends_on"] = "chi"
    dset = create_nxtransformation(
        nxtransformations,
        "chi",
        positioners["chi"],
        "deg",
        "rotation",
        [1, 0, 0],
    )
    dset.attrs["depends_on"] = "omega"
    dset = create_nxtransformation(
        nxtransformations,
        "omega",
        positioners["omega"],
        "deg",
        "rotation",
        [0, -1, 0],
    )
    dset.attrs["depends_on"] = "x"
    dset = create_nxtransformation(
        nxtransformations,
        "x",
        positioners["x"],
        "mm",
        "translation",
        [1, 0, 0],
    )
    dset.attrs["depends_on"] = "y"
    dset = create_nxtransformation(
        nxtransformations,
        "y",
        positioners["y"],
        "mm",
        "translation",
        [0, 1, 0],
    )
    dset.attrs["depends_on"] = "z"
    dset = create_nxtransformation(
        nxtransformations,
        "z",
        positioners["z"],
        "mm",
        "translation",
        [0, 0, 1],
    )
    add_coordinate_system(nxtransformations, dset)


def add_nxsample(parent, name, point_url, with_cradle=True):
    nxsample = parent.create_group("sample")
    nxsample.attrs["NX_class"] = "NXsample"
    nxsample["name"] = name
    with h5context(point_url) as point:
        scan = point.parent.parent
        positioners = id15_positioners(scan, with_cradle=with_cradle)
    position_sample(nxsample, positioners)


def add_gauge_volume(parent):
    bip = parent.create_group("beam_intensity_profile")
    bip.attrs["NX_class"] = "NXcollection"

    bip["primary_vertical_type"] = "slit"
    bip["primary_vertical_full_width"] = 100.0
    bip["primary_vertical_full_width"].attrs["units"] = "um"
    bip["primary_vertical_width"] = 100.0
    bip["primary_vertical_width"].attrs["units"] = "um"
    bip["primary_vertical_distance"] = float("nan")
    bip["primary_vertical_distance"].attrs["units"] = "mm"

    bip["primary_horizontal_type"] = "slit"
    bip["primary_horizontal_full_width"] = 100.0
    bip["primary_horizontal_full_width"].attrs["units"] = "um"
    bip["primary_horizontal_width"] = 100.0
    bip["primary_horizontal_width"].attrs["units"] = "um"
    bip["primary_horizontal_distance"] = float("nan")
    bip["primary_horizontal_distance"].attrs["units"] = "mm"

    bip["secondary_horizontal_type"] = "slit"
    bip["secondary_horizontal_full_width"] = 100.0
    bip["secondary_horizontal_full_width"].attrs["units"] = "um"
    bip["secondary_horizontal_width"] = 100.0
    bip["secondary_horizontal_width"].attrs["units"] = "um"
    bip["secondary_horizontal_distance"] = float("nan")
    bip["secondary_horizontal_distance"].attrs["units"] = "mm"


def iter_esrf_point_fitresults(point_url, separate=True, with_cradle=True):
    with h5context(point_url) as point:
        scan = point.parent.parent
        infos = scan["infos"]
        positioners = id15_positioners(scan, with_cradle)

        dataaxes = ["channels"]
        datasignals = ["diffractogram", "fit", "background", "residuals"]
        datanamemap = {
            "diffractogram": "raw_data",
            "fit": "fitted_data",
            "residuals": "residual",
        }

        paramcolumns = [
            "area",
            "center",
            "fwhm_left",
            "fwhm_right",
            "form_factor",
            "goodness_of_fit",
        ]
        errorcolumns = ["area", "center", "fwhm_left", "fwhm_right", "form_factor"]

        units = {
            "azimuth": "deg",
            "chi": "deg",
            "phi": "deg",
            "omega": "deg",
            "x": "mm",
            "y": "mm",
            "z": "mm",
            "center": "keV",
        }

        detectornames = ["horizontal", "vertical"]
        detectornamemap = {"horizontal": "mca2_det0", "vertical": "mca2_det1"}
        paramnames = ["fitParamsHD", "fitParamsVD"]
        errornames = ["uncertaintyFitParamsHD", "uncertaintyFitParamsVD"]
        det_azims = [ID15_det0_azimuth, ID15_det1_azimuth]
        det_polars = [ID15_det0_polar, ID15_det1_polar]
        energy_calibs = [ID15_det0_calib, ID15_det1_calib]

        for (
            detectorname,
            paramname,
            errorname,
            det_azim,
            det_polar,
            energy_calib,
        ) in zip(
            detectornames, paramnames, errornames, det_azims, det_polars, energy_calibs
        ):
            idx = 0
            orgdetname = detectornamemap[detectorname]
            for i, npeaks in enumerate(infos["nbPeaksInBoxes"]):
                ##################
                fitdata = point[f"fitLine_{i:04d}"][detectorname]  # {...: nchannels}
                signals = OrderedDict(
                    (name, fitdata[datanamemap.get(name, name)][()])
                    for name in datasignals
                )
                axes = OrderedDict(
                    (name, fitdata[datanamemap.get(name, name)][()])
                    for name in dataaxes
                )
                nxdataparams = {
                    "signals": signals,
                    "axes": axes,
                    "interpretation": "spectrum",
                    "subgroups": "fit",
                    "default": True,
                }
                if separate:
                    nxdatainfo = {
                        "mergename": f"diffractogram_{orgdetname}",
                        "mergetitle": f"Diffractogram (det_azim={det_azim} deg)",
                    }
                    nxdataparams["name"] = f"diffractogram_{orgdetname}_roi{i:04d}"
                    nxdataparams[
                        "title"
                    ] = f"Diffractogram (det_azim={det_azim} deg, ROI {i})"
                    nxdataparams["subgroups"] = (
                        "fit",
                        f"diffractogram_{orgdetname}",
                        orgdetname,
                    )
                else:
                    nxdatainfo = {
                        # "stackaxisvalue": f"det_azim={det_azim} deg, roi{i:04d}"
                        "stackaxisvalue": det_azim
                    }
                    nxdataparams["name"] = "diffractogram"
                    nxdataparams["title"] = "Diffractogram"
                yield nxdatainfo, nxdataparams
                ##################

                fitparams = point["fitParams"][paramname][
                    idx : idx + npeaks, :
                ]  # npeaks x nparams
                fiterrors = point["fitParams"][errorname][
                    idx : idx + npeaks, :
                ]  # npeaks x nerrors-1
                idx += npeaks
                signals = OrderedDict(
                    (name, values) for name, values in zip(paramcolumns, fitparams.T)
                )
                errors = OrderedDict(
                    (name, values) for name, values in zip(errorcolumns, fiterrors.T)
                )

                energy = (
                    signals["center"] ** 2 * energy_calib[0]
                    + signals["center"] * energy_calib[1]
                    + energy_calib[2]
                )
                energy_errors = (
                    numpy.sqrt((2 * signals["center"] * energy_calib[0]) ** 2)
                    * errors["center"]
                )

                fitunits = {k: v for k, v in units.items() if k != "center"}
                nxdataparams = {
                    "signals": signals,
                    "errors": errors,
                    "units": fitunits,
                    "subgroups": "fit",
                    "default": False,
                    "other_datasets": {"center_type": "channel"},
                }
                if separate:
                    nxdatainfo = {
                        "mergename": "peak_parameters",
                        "mergetitle": "split pseudo-voigt",
                    }
                    nxdataparams["name"] = f"peak_parameters_{orgdetname}_roi{i:04d}"
                    nxdataparams["title"] = "split pseudo-voigt"
                    nxdataparams["subgroups"] = "fit", "peak_parameters", orgdetname
                else:
                    nxdatainfo = {
                        "stackaxisvalue": f"det_azim={det_azim} deg, roi{i:04d}"
                    }
                    nxdataparams["name"] = "peak_parameters"
                    nxdataparams["title"] = "split pseudo-voigt"
                yield nxdatainfo, nxdataparams
                ##################

                signals = {
                    "A0": numpy.full(npeaks, float("nan")),
                    "A1": numpy.full(npeaks, float("nan")),
                }

                nxdataparams = {
                    "signals": signals,
                    "subgroups": "fit",
                    "default": False,
                }
                if separate:
                    nxdatainfo = {
                        "mergename": "background_parameters",
                        "mergetitle": "linear",
                    }
                    nxdataparams[
                        "name"
                    ] = f"background_parameters_{orgdetname}_roi{i:04d}"
                    nxdataparams["title"] = "linear"
                    nxdataparams["subgroups"] = (
                        "fit",
                        "background_parameters",
                        orgdetname,
                    )
                else:
                    nxdatainfo = {
                        "stackaxisvalue": f"det_azim={det_azim} deg, roi{i:04d}"
                    }
                    nxdataparams["name"] = "background_parameters"
                    nxdataparams["title"] = "linear"
                yield nxdatainfo, nxdataparams
                ##################

                signals = OrderedDict(
                    (name, numpy.full(npeaks, value))
                    for name, value in positioners.items()
                )

                nxdataparams = {
                    "signals": signals,
                    "units": units,
                    "subgroups": "fit",
                    "default": False,
                }
                if separate:
                    nxdatainfo = {
                        "mergename": "extra",
                        "mergetitle": "Extra parameters",
                    }
                    nxdataparams["name"] = f"extra_{orgdetname}_roi{i:04d}"
                    nxdataparams[
                        "title"
                    ] = f"Extra parameters (det_azim={det_azim} deg, ROI {i})"
                    nxdataparams["subgroups"] = "fit", "extra", orgdetname
                else:
                    nxdatainfo = {
                        "stackaxisvalue": f"det_azim={det_azim} deg, roi{i:04d}"
                    }
                    nxdataparams["name"] = "extra"
                    nxdataparams["title"] = "Extra parameters"

                # yield nxdatainfo, nxdataparams
                ##################

                signals = OrderedDict()
                signals["h"] = ID15_h[i : i + npeaks]
                signals["k"] = ID15_k[i : i + npeaks]
                signals["l"] = ID15_l[i : i + npeaks]

                angles = [
                    positioners["phi"],
                    positioners["chi"],
                    positioners["omega"],
                    det_azim,
                    det_polar,
                ]
                qx, qy, qz = compute_qs(angles)  # Q in the sample referene frame
                signals["qx"] = numpy.full(npeaks, qx)
                signals["qy"] = numpy.full(npeaks, qy)
                signals["qz"] = numpy.full(npeaks, qz)
                signals["sx"] = numpy.full(npeaks, positioners["x"])
                signals["sy"] = numpy.full(npeaks, positioners["y"])
                signals["sz"] = numpy.full(npeaks, positioners["z"])
                signals["center"] = energy
                signals["lattice"] = as_vlen_string(["cubic"] * npeaks)
                signals["phase_name"] = as_vlen_string(["stainless steal"] * npeaks)

                nxdataparams = {
                    "signals": signals,
                    "units": units,
                    "errors": {"center": energy_errors},
                    "other_datasets": {"center_type": "energy"},
                    "subgroups": None,
                    "default": False,
                }
                if separate:
                    nxdatainfo = {
                        "mergename": "peaks",
                        "mergetitle": "Peak parameters",
                    }
                    nxdataparams["name"] = f"peaks_{orgdetname}_roi{i:04d}"
                    nxdataparams[
                        "title"
                    ] = f"Peak parameters (det_azim={det_azim} deg, ROI {i})"
                    nxdataparams["subgroups"] = "peaks", orgdetname
                else:
                    nxdatainfo = {
                        "stackaxisvalue": f"det_azim={det_azim} deg, roi{i:04d}"
                    }
                    nxdataparams["name"] = "peaks"
                    nxdataparams["title"] = "Peak parameters"
                yield nxdatainfo, nxdataparams


def iter_strarray(dset):
    for name in dset:
        yield name.decode()


def iter_salsa_point_fitresults(point_url, shared):
    with h5context(point_url) as point:
        units = {"scat_angle": "deg"}

        _, y, yerrors = point["Diffractogram"][()].T
        _, yfit, yfiterrors = point["Fitted_profile"][()].T

        axes = OrderedDict()
        signals = OrderedDict()
        errors = OrderedDict()

        axes["scat_angle"] = h5py.SoftLink(shared["scat_angle"].name)
        signals["diffractogram"] = y
        signals["fit"] = yfit
        errors["diffractogram"] = yerrors
        errors["fit"] = yfiterrors
        signals["background"] = h5py.SoftLink(shared["background"].name)

        nxdataparams = {
            "name": "diffractogram",
            "title": "Diffractogram",
            "signals": signals,
            "errors": errors,
            "axes": axes,
            "interpretation": "spectrum",
            "units": units,
            "subgroups": "fit",
            "default": True,
        }

        yield nxdataparams
        #################

        namemap = {"position": "center"}

        units = dict(
            zip(
                iter_strarray(point["Fit_parameter_names"]),
                point["Fit_parameter_units"],
            )
        )
        signals = OrderedDict(
            zip(
                iter_strarray(point["Fit_parameter_names"]),
                point["Fit_parameter_values"],
            )
        )
        errors = OrderedDict(
            zip(
                iter_strarray(point["Fit_parameter_names"]),
                point["Fit_parameter_uncertainties"],
            )
        )
        units = {namemap.get(k, k): v for k, v in units.items()}
        signals = {namemap.get(k, k): v for k, v in signals.items()}
        errors = {namemap.get(k, k): v for k, v in errors.items()}

        nxdataparams = {
            "name": "peak_parameters",
            "title": point["Fit_function"][()],
            "signals": signals,
            "errors": errors,
            "units": units,
            "subgroups": "fit",
            "default": False,
        }

        ttheta = signals["center"]
        ttheta_errors = errors["center"]

        yield nxdataparams
        #################

        units = dict(
            zip(iter_strarray(point["Scan_types"]), iter_strarray(point["Scan_units"]))
        )
        signals = OrderedDict(
            zip(iter_strarray(point["Scan_types"]), point["Scan_values"])
        )
        nxdataparams = {
            "name": "extra",
            "title": "Extra parameters",
            "signals": signals,
            "units": units,
            "subgroups": "fit",
            "default": False,
        }

        yield nxdataparams
        #################

        signals = {
            "h": 3,
            "k": 1,
            "l": 1,
            "lattice": "trigonal",
            "space_group": "R3c",
            "phase_name": "hematite",
            "sx": signals["X_sample"],
            "sy": signals["Y_sample"],
            "sz": signals["Z_sample"],
            "qx": signals["qx"],
            "qy": signals["qy"],
            "qz": signals["qz"],
            "center": ttheta,
        }

        errors = {"center": ttheta_errors}

        units = {"center": "deg"}

        nxdataparams = {
            "name": "peaks",
            "title": "Peaks parameters",
            "signals": signals,
            "errors": errors,
            "units": units,
            "other_datasets": {"center_type": "two-theta"},
            "subgroups": None,
            "default": False,
        }

        yield nxdataparams
        #################


def create_subgroups(parent, subgroups=None, up=0):
    if subgroups:
        if isinstance(subgroups, str):
            subgroups = [subgroups]
        _parent = parent
        for subgroup in subgroups:
            _parent.attrs["default"] = subgroup
            _parent = _parent.require_group(subgroup)
            _parent.attrs.setdefault("NX_class", "NXcollection")
        if up:
            subgroups = subgroups[:-up]
        for subgroup in subgroups:
            parent = parent[subgroup]
    return parent


def save_nxdata(
    parent,
    name,
    signals=None,
    axes=None,
    errors=None,
    units=None,
    title=None,
    interpretation=None,
    other_datasets=None,
    subgroups=None,
    default=False,
):
    parent = create_subgroups(parent, subgroups=subgroups)
    if default:
        parent.attrs["default"] = name
    nxdata = parent.create_group(name)
    nxdata.attrs["NX_class"] = "NXdata"
    if not units:
        units = dict()
    if signals:
        snames = [
            k
            for k, v in signals.items()
            if not (isinstance(v, numpy.ndarray) and v.dtype == VLEN_DTYPE)
        ]
        nxdata.attrs["signal"] = snames[0]
        nxdata.attrs["auxiliary_signals"] = snames[1:]
        for name, values in signals.items():
            nxdata[name] = values
            u = units.get(name)
            if u:
                nxdata[name].attrs["units"] = u
            if interpretation:
                nxdata[name].attrs["interpretation"] = interpretation
    if axes:
        nxdata.attrs["axes"] = list(axes)
        for name, values in axes.items():
            nxdata[name] = values
            u = units.get(name)
            if u:
                nxdata[name].attrs["units"] = u
    if errors:
        for name, values in errors.items():
            nxdata[f"{name}_errors"] = values
            u = units.get(name)
            if u:
                nxdata[f"{name}_errors"].attrs["units"] = u
            if interpretation:
                nxdata[f"{name}_errors"].attrs["interpretation"] = interpretation
    if title:
        nxdata["title"] = title
    if other_datasets:
        for name, value in other_datasets.items():
            nxdata[name] = value
    return nxdata


def merge_datasets(parent, name, dsets):
    off = numpy.zeros(dsets[0].ndim, dtype=int)
    sources = list()
    for dseti in dsets:
        vsource = h5py.VirtualSource(dseti)
        n = numpy.array(dseti.shape, dtype=int)
        idx = tuple(slice(offi, offi + ni) for offi, ni in zip(off, n))
        sources.append((idx, vsource))
        off += n
    total_shape = tuple(off)
    dtype = list({dset.dtype for dset in dsets})[0]
    if dtype == VLEN_DTYPE:
        fillvalue = None  # as_vlen_string("")
    else:
        fillvalue = numpy.nan

    layout = h5py.VirtualLayout(shape=total_shape, dtype=dtype)
    for idx, vsource in sources:
        layout[idx] = vsource

    dset = parent.create_virtual_dataset(name, layout, fillvalue=fillvalue)
    dset.attrs.update(dsets[0].attrs)


def merge_nxdatas(parent, name, nxdatas, nxdataparams, title):
    subgroups = nxdataparams[0].get("subgroups")
    parent = create_subgroups(parent, subgroups=subgroups, up=2)
    default = nxdataparams[0].get("default")
    if default:
        parent.attrs["default"] = name
    nxdata = parent.require_group(name)
    nxdata.attrs.update(nxdatas[0].attrs)
    dataset_names = list(nxdatas[0])
    notmerged = ["title"]

    for dataset_name in dataset_names:
        if dataset_name not in notmerged:
            # merge
            dsets = [nxdatai[dataset_name] for nxdatai in nxdatas]
            merge_datasets(nxdata, dataset_name, dsets)
        elif dataset_name not in nxdata:
            # take the first value
            if dataset_name == "title" and title:
                value = title
            else:
                value = nxdatas[0][dataset_name]
            nxdata[dataset_name] = value


def stack_axis(values1, values2):
    values = numpy.unique(numpy.append(values1, values2))
    idx1 = numpy.where(numpy.in1d(values, values1))[0].tolist()
    idx2 = numpy.where(numpy.in1d(values, values2))[0].tolist()
    return values, idx1, idx2


def stack_signal(values, addvalues, ndim1, idx1, idx2):
    ndim0 = values.shape[0] + 1
    shape = (ndim0, ndim1)
    stack = numpy.full(shape, numpy.nan)
    stack[:-1, idx1] = values
    stack[-1, idx2] = addvalues
    return stack


def stack_nxdatas(nxdatas, nxdataname, nxdataadd, stackname, stackvalue):
    bstack = "axes" in nxdataadd
    groups = ["signals"]
    if "errors" in nxdataadd:
        groups.append("errors")

    if nxdataname not in nxdatas:
        nxdatas[nxdataname] = nxdataadd
        if bstack:
            old = nxdataadd["axes"]
            nxdataadd["axes"] = OrderedDict()
            nxdataadd["axes"][stackname] = [stackvalue]
            nxdataadd["axes"].update(old)
            for k in groups:
                for name in nxdataadd[k]:
                    nxdataadd[k][name] = nxdataadd[k][name][numpy.newaxis, :]
        return

    nxdatastack = nxdatas[nxdataname]

    if bstack:
        nxdatastack["axes"][stackname].append(stackvalue)
        channels, idx1, idx2 = stack_axis(
            nxdatastack["axes"]["channels"], nxdataadd["axes"]["channels"]
        )
        nxdatastack["axes"]["channels"] = channels
        for k in groups:
            for name in list(nxdatastack[k]):
                values1 = nxdatastack[k][name]
                values2 = nxdataadd[k][name]
                nxdatastack[k][name] = stack_signal(
                    values1, values2, channels.size, idx1, idx2
                )
    else:
        for k in groups:
            for name in list(nxdatastack.get(k, list())):
                nxdatastack[k][name] = numpy.append(
                    nxdatastack[k][name], nxdataadd[k][name]
                )


def create_id15_edd(nxinstrument, name, distance, polar, azimuth):
    nxdetector = nxinstrument.create_group(name)
    nxdetector.attrs["NX_class"] = "NXdetector"
    nxdetector["type"] = "MCA"
    nxdetector["description"] = "Ge MCA"

    position_point_detector(nxdetector, distance, polar, azimuth)

    nxslit = nxinstrument.create_group(f"{name}_slits")
    nxslit.attrs["NX_class"] = "NXslit"
    nxslit["x_gap"] = 100.0
    nxslit["x_gap"].attrs["units"] = "um"
    nxslit["y_gap"] = 100.0
    nxslit["y_gap"].attrs["units"] = "um"

    position_point_detector_slits(nxslit, distance, polar, azimuth)


def create_id15_source(nxinstrument):
    nxsource = nxinstrument.create_group("source")
    nxsource.attrs["NX_class"] = "NXsource"
    nxsource["type"] = "Synchrotron X-ray Source"
    nxsource["probe"] = "X-ray"

    nxslit = nxinstrument.create_group("primary_slits")
    nxslit.attrs["NX_class"] = "NXslit"
    nxslit["x_gap"] = 100.0
    nxslit["x_gap"].attrs["units"] = "um"
    nxslit["y_gap"] = 100.0
    nxslit["y_gap"].attrs["units"] = "um"

    position_source_slits(nxslit)


def create_salsa_psd(nxinstrument, name):
    nxdetector = nxinstrument.create_group(name)
    nxdetector.attrs["NX_class"] = "NXdetector"
    nxdetector["description"] = "PSD 2D"


# horizontal
ID15_det0_polar = 5.02876
ID15_det0_azimuth = -90.0
ID15_det0_distance = float("nan")
ID15_det0_calib = [4.04408e-08, 0.0748839, 0.103744]

# vertical
ID15_det1_polar = 4.91329
ID15_det1_azimuth = 0.0
ID15_det1_distance = float("nan")
ID15_det1_calib = [2.22669e-08, 0.0749473, 0.0574087]

ID15_d0 = [1.267594775, 1.080594133, 1.034951928, 0.822132144, 0.801651573]
ID15_hkl = ["(220)", "(311)", "(222)", "(331)", "(420)"]
ID15_h = [2, 3, 2, 3, 4]
ID15_k = [2, 1, 2, 3, 2]
ID15_l = [0, 1, 2, 1, 0]

ID15_samplename = "BAIII_AB_4_1"


def create_id15_nxinstrument(nxentry):
    nxinstrument = nxentry.create_group("instrument")
    nxinstrument.attrs["NX_class"] = "NXinstrument"
    nxinstrument["name"] = "ESRF-ID15"
    nxinstrument["name"].attrs["short_name"] = "ID15"
    create_id15_edd(
        nxinstrument,
        "mca2_det1",
        ID15_det1_distance,
        ID15_det1_polar,
        ID15_det1_azimuth,
    )
    create_id15_edd(
        nxinstrument,
        "mca2_det0",
        ID15_det0_distance,
        ID15_det0_polar,
        ID15_det0_azimuth,
    )
    create_id15_source(nxinstrument)


def create_salsa_nxinstrument(nxentry):
    nxinstrument = nxentry.create_group("instrument")
    nxinstrument.attrs["NX_class"] = "NXinstrument"
    nxinstrument["name"] = "ILL-SALSA"
    nxinstrument["name"].attrs["short_name"] = "SALSA"
    create_salsa_psd(nxinstrument, "psd")


def prepare_id15_nxentry(
    nxentry,
    point_url,
    with_cradle=True,
    sample_name=ID15_samplename,
    exp_identifier="ihme10",
):
    nxentry.attrs["NX_class"] = "NXentry"
    nxentry["definition"] = "NXstress"
    nxentry["experiment_identifier"] = exp_identifier
    nxentry["collection_identifier"] = [
        s for s in point_url.split("::")[-1].split("/") if s
    ][0]
    nxentry["experiment_description"] = "energy-dispersive X-ray powder diffraction"
    nxentry["diffraction_type"] = "energy"
    nxentry["start_time"] = datetime.now().astimezone().isoformat()
    nxentry["end_time"] = datetime.now().astimezone().isoformat()
    create_id15_nxinstrument(nxentry)
    add_nxprocess(nxentry, "fit")
    add_nxsample(nxentry, sample_name, point_url, with_cradle)
    add_gauge_volume(nxentry)


def prepare_salsa_nxentry(nxentry):
    nxentry.attrs["NX_class"] = "NXentry"
    nxentry["definition"] = "NXstress"
    nxentry["experiment_identifier"] = "10.5291/ILL-DATA.INTER-560"
    nxentry["collection_identifier"] = "mycollectionnumber"
    create_salsa_nxinstrument(nxentry)
    add_nxprocess(nxentry, "fit")


def esrf_id15_example1_as_nxstress(nxentry, point_url, with_cradle: bool, **kwargs):
    prepare_id15_nxentry(nxentry, point_url, with_cradle=with_cradle, **kwargs)

    merge = dict()
    titles = dict()
    for nxdatainfo, nxdataparams in iter_esrf_point_fitresults(
        point_url, separate=True, with_cradle=with_cradle
    ):
        mergename = nxdatainfo["mergename"]
        titles[mergename] = nxdatainfo["mergetitle"]
        nxdatas, nxdataparamslst = merge.setdefault(mergename, [list(), list()])
        nxdata = save_nxdata(nxentry, **nxdataparams)
        nxdatas.append(nxdata)
        nxdataparamslst.append(nxdataparams)

    for mergename, (nxdatas, nxdataparams) in merge.items():
        merge_nxdatas(nxentry, mergename, nxdatas, nxdataparams, titles[mergename])


def esrf_id15_example2_as_nxstress(nxentry, point_url):
    prepare_id15_nxentry(nxentry, point_url)
    nxdatas = dict()
    for nxdatainfo, nxdataparams in iter_esrf_point_fitresults(
        point_url, separate=False
    ):
        nxdataname = nxdataparams["name"]
        stack_nxdatas(
            nxdatas, nxdataname, nxdataparams, "patterns", nxdatainfo["stackaxisvalue"]
        )
    for nxdataparams in nxdatas.values():
        save_nxdata(nxentry, **nxdataparams)


def ill_salsa_example1_as_nxstress(nxentry, point_url, shared):
    prepare_salsa_nxentry(nxentry)
    for nxdataparams in iter_salsa_point_fitresults(point_url, shared):
        save_nxdata(nxentry, **nxdataparams)


def ill_salsa_header_as_nxstress(nxcollection, header_url):
    nxcollection.attrs["NX_class"] = "NXcollection"
    with h5context(header_url) as group:
        scat_angle, background = group.parent["General_info"]["Bgr_profile"][()].T
        nxcollection["scat_angle"] = scat_angle
        nxcollection["background"] = background

    add_nxprocess(nxcollection, "fit_background")
    save_nxdata(nxcollection["fit_background"], "background_parameters", title="shape")


def id15_point_urls(filenames: Sequence[str]):
    for filename in filenames:
        with h5py.File(filename, mode="r") as nxroot:
            for name in nxroot:
                # if "0001_42.1" in name or "0002_32.1" in name:
                # if "0001_52.1" in name or "0002_42.1" in name:
                # if "0002_42.1" in name:
                if True:
                    point_url = f"{filename}::/{name}/fit/0000"
                    yield name, point_url


def example_id15_point_urls():
    return id15_point_urls(
        [
            os.path.join(
                ".", "raw_data", "fit_testNexus_OR1_dataset_0001_ihme10_BAIII_AB_4_1.h5"
            ),
            os.path.join(
                ".", "raw_data", "fit_testNexus_OR2_dataset_0002_ihme10_BAIII_AB_4_1.h5"
            ),
        ]
    )


def esrf_id15_example1():
    with h5py.File("./fmt_data/esrf_id15_example1.h5", mode="w") as nxroot:
        nxroot.attrs["NX_class"] = "NXroot"
        for name, point_url in example_id15_point_urls():
            nxentry = nxroot.create_group(name)
            nxroot.attrs["default"] = name
            esrf_id15_example1_as_nxstress(nxentry, point_url, with_cradle=True)


def esrf_id15_example2():
    with h5py.File("./fmt_data/esrf_id15_example2.h5", mode="w") as nxroot:
        nxroot.attrs["NX_class"] = "NXroot"
        for name, point_url in example_id15_point_urls():
            nxentry = nxroot.create_group(name)
            nxroot.attrs["default"] = name
            esrf_id15_example2_as_nxstress(nxentry, point_url)


def ill_salsa_example1():
    odatadir = "./raw_data"
    with h5py.File("./fmt_data/ill_salsa_example1.h5", mode="w") as nxroot:
        nxroot.attrs["NX_class"] = "NXroot"

        header_url = os.path.join(odatadir, "salsa.h5::/General_info")
        shared = nxroot.create_group("Shared")
        ill_salsa_header_as_nxstress(shared, header_url)

        for i in range(1, 3):
            name = f"Point_{i:04d}"
            point_url = os.path.join(odatadir, f"salsa.h5::/{name}")
            nxentry = nxroot.create_group(name)
            nxroot.attrs["default"] = name
            ill_salsa_example1_as_nxstress(nxentry, point_url, shared)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--example", action="store_true")
    parser.add_argument("--with_cradle", action="store_true")
    parser.add_argument("--sample")
    parser.add_argument("--exp_identifier")
    parser.add_argument("filenames", nargs="*")

    options = parser.parse_args(sys.argv[1:])

    if options.example:
        esrf_id15_example1()
        esrf_id15_example2()
        ill_salsa_example1()

    if options.filenames == 0:
        raise ValueError("Please give a filename as input")

    dir_name = os.path.dirname(options.filenames[0])

    with h5py.File(os.path.join(dir_name, "nx_stress.h5"), mode="w") as nxroot:
        nxroot.attrs["NX_class"] = "NXroot"
        for name, point_url in id15_point_urls(options.filenames):
            nxentry = nxroot.create_group(name)
            nxroot.attrs["default"] = name

            kwargs = {}
            if options.sample:
                kwargs["sample_name"] = options.sample
            if options.exp_identifier:
                kwargs["exp_identifier"] = options.exp_identifier

            esrf_id15_example1_as_nxstress(
                nxentry, point_url, with_cradle=bool(options.with_cradle), **kwargs
            )


if __name__ == "__main__":
    main()
